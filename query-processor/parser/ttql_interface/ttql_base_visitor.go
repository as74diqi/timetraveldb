// Code generated from java-escape by ANTLR 4.11.1. DO NOT EDIT.

package ttql_interface // TTQL
import "github.com/antlr/antlr4/runtime/Go/antlr/v4"

type BaseTTQLVisitor struct {
	*antlr.BaseParseTreeVisitor
}

func (v *BaseTTQLVisitor) VisitTtQL(ctx *TtQLContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseTTQLVisitor) VisitTtQL_Statement(ctx *TtQL_StatementContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseTTQLVisitor) VisitTtQL_Query(ctx *TtQL_QueryContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseTTQLVisitor) VisitTtQL_TimeClause(ctx *TtQL_TimeClauseContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseTTQLVisitor) VisitOC_Cypher(ctx *OC_CypherContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseTTQLVisitor) VisitOC_Statement(ctx *OC_StatementContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseTTQLVisitor) VisitOC_Query(ctx *OC_QueryContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseTTQLVisitor) VisitOC_RegularQuery(ctx *OC_RegularQueryContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseTTQLVisitor) VisitOC_Union(ctx *OC_UnionContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseTTQLVisitor) VisitOC_SingleQuery(ctx *OC_SingleQueryContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseTTQLVisitor) VisitOC_SinglePartQuery(ctx *OC_SinglePartQueryContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseTTQLVisitor) VisitOC_MultiPartQuery(ctx *OC_MultiPartQueryContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseTTQLVisitor) VisitOC_UpdatingClause(ctx *OC_UpdatingClauseContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseTTQLVisitor) VisitOC_ReadingClause(ctx *OC_ReadingClauseContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseTTQLVisitor) VisitOC_Match(ctx *OC_MatchContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseTTQLVisitor) VisitOC_Unwind(ctx *OC_UnwindContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseTTQLVisitor) VisitOC_Merge(ctx *OC_MergeContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseTTQLVisitor) VisitOC_MergeAction(ctx *OC_MergeActionContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseTTQLVisitor) VisitOC_Create(ctx *OC_CreateContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseTTQLVisitor) VisitOC_Set(ctx *OC_SetContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseTTQLVisitor) VisitOC_SetItem(ctx *OC_SetItemContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseTTQLVisitor) VisitOC_Delete(ctx *OC_DeleteContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseTTQLVisitor) VisitOC_Remove(ctx *OC_RemoveContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseTTQLVisitor) VisitOC_RemoveItem(ctx *OC_RemoveItemContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseTTQLVisitor) VisitOC_InQueryCall(ctx *OC_InQueryCallContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseTTQLVisitor) VisitOC_StandaloneCall(ctx *OC_StandaloneCallContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseTTQLVisitor) VisitOC_YieldItems(ctx *OC_YieldItemsContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseTTQLVisitor) VisitOC_YieldItem(ctx *OC_YieldItemContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseTTQLVisitor) VisitOC_With(ctx *OC_WithContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseTTQLVisitor) VisitOC_Return(ctx *OC_ReturnContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseTTQLVisitor) VisitOC_ProjectionBody(ctx *OC_ProjectionBodyContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseTTQLVisitor) VisitOC_ProjectionItems(ctx *OC_ProjectionItemsContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseTTQLVisitor) VisitOC_ProjectionItem(ctx *OC_ProjectionItemContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseTTQLVisitor) VisitOC_Order(ctx *OC_OrderContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseTTQLVisitor) VisitOC_Skip(ctx *OC_SkipContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseTTQLVisitor) VisitOC_Limit(ctx *OC_LimitContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseTTQLVisitor) VisitOC_SortItem(ctx *OC_SortItemContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseTTQLVisitor) VisitOC_Where(ctx *OC_WhereContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseTTQLVisitor) VisitOC_Pattern(ctx *OC_PatternContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseTTQLVisitor) VisitOC_PatternPart(ctx *OC_PatternPartContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseTTQLVisitor) VisitOC_AnonymousPatternPart(ctx *OC_AnonymousPatternPartContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseTTQLVisitor) VisitOC_PatternElement(ctx *OC_PatternElementContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseTTQLVisitor) VisitOC_RelationshipsPattern(ctx *OC_RelationshipsPatternContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseTTQLVisitor) VisitOC_NodePattern(ctx *OC_NodePatternContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseTTQLVisitor) VisitOC_PatternElementChain(ctx *OC_PatternElementChainContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseTTQLVisitor) VisitOC_RelationshipPattern(ctx *OC_RelationshipPatternContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseTTQLVisitor) VisitOC_RelationshipDetail(ctx *OC_RelationshipDetailContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseTTQLVisitor) VisitOC_Properties(ctx *OC_PropertiesContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseTTQLVisitor) VisitOC_RelationshipTypes(ctx *OC_RelationshipTypesContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseTTQLVisitor) VisitOC_NodeLabels(ctx *OC_NodeLabelsContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseTTQLVisitor) VisitOC_NodeLabel(ctx *OC_NodeLabelContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseTTQLVisitor) VisitOC_RangeLiteral(ctx *OC_RangeLiteralContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseTTQLVisitor) VisitOC_LabelName(ctx *OC_LabelNameContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseTTQLVisitor) VisitOC_RelTypeName(ctx *OC_RelTypeNameContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseTTQLVisitor) VisitOC_PropertyExpression(ctx *OC_PropertyExpressionContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseTTQLVisitor) VisitOC_Expression(ctx *OC_ExpressionContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseTTQLVisitor) VisitOC_OrExpression(ctx *OC_OrExpressionContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseTTQLVisitor) VisitOC_XorExpression(ctx *OC_XorExpressionContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseTTQLVisitor) VisitOC_AndExpression(ctx *OC_AndExpressionContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseTTQLVisitor) VisitOC_NotExpression(ctx *OC_NotExpressionContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseTTQLVisitor) VisitOC_ComparisonExpression(ctx *OC_ComparisonExpressionContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseTTQLVisitor) VisitOC_PartialComparisonExpression(ctx *OC_PartialComparisonExpressionContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseTTQLVisitor) VisitOC_StringListNullPredicateExpression(ctx *OC_StringListNullPredicateExpressionContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseTTQLVisitor) VisitOC_StringPredicateExpression(ctx *OC_StringPredicateExpressionContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseTTQLVisitor) VisitOC_ListPredicateExpression(ctx *OC_ListPredicateExpressionContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseTTQLVisitor) VisitOC_NullPredicateExpression(ctx *OC_NullPredicateExpressionContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseTTQLVisitor) VisitOC_AddOrSubtractExpression(ctx *OC_AddOrSubtractExpressionContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseTTQLVisitor) VisitOC_MultiplyDivideModuloExpression(ctx *OC_MultiplyDivideModuloExpressionContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseTTQLVisitor) VisitOC_PowerOfExpression(ctx *OC_PowerOfExpressionContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseTTQLVisitor) VisitOC_UnaryAddOrSubtractExpression(ctx *OC_UnaryAddOrSubtractExpressionContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseTTQLVisitor) VisitOC_ListOperatorExpression(ctx *OC_ListOperatorExpressionContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseTTQLVisitor) VisitOC_PropertyOrLabelsExpression(ctx *OC_PropertyOrLabelsExpressionContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseTTQLVisitor) VisitOC_PropertyLookup(ctx *OC_PropertyLookupContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseTTQLVisitor) VisitOC_Atom(ctx *OC_AtomContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseTTQLVisitor) VisitOC_CaseExpression(ctx *OC_CaseExpressionContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseTTQLVisitor) VisitOC_CaseAlternative(ctx *OC_CaseAlternativeContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseTTQLVisitor) VisitOC_ListComprehension(ctx *OC_ListComprehensionContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseTTQLVisitor) VisitOC_PatternComprehension(ctx *OC_PatternComprehensionContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseTTQLVisitor) VisitOC_Quantifier(ctx *OC_QuantifierContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseTTQLVisitor) VisitOC_FilterExpression(ctx *OC_FilterExpressionContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseTTQLVisitor) VisitOC_PatternPredicate(ctx *OC_PatternPredicateContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseTTQLVisitor) VisitOC_ParenthesizedExpression(ctx *OC_ParenthesizedExpressionContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseTTQLVisitor) VisitOC_IdInColl(ctx *OC_IdInCollContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseTTQLVisitor) VisitOC_FunctionInvocation(ctx *OC_FunctionInvocationContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseTTQLVisitor) VisitOC_FunctionName(ctx *OC_FunctionNameContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseTTQLVisitor) VisitOC_ExistentialSubquery(ctx *OC_ExistentialSubqueryContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseTTQLVisitor) VisitOC_ExplicitProcedureInvocation(ctx *OC_ExplicitProcedureInvocationContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseTTQLVisitor) VisitOC_ImplicitProcedureInvocation(ctx *OC_ImplicitProcedureInvocationContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseTTQLVisitor) VisitOC_ProcedureResultField(ctx *OC_ProcedureResultFieldContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseTTQLVisitor) VisitOC_ProcedureName(ctx *OC_ProcedureNameContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseTTQLVisitor) VisitOC_Namespace(ctx *OC_NamespaceContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseTTQLVisitor) VisitOC_Variable(ctx *OC_VariableContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseTTQLVisitor) VisitOC_Literal(ctx *OC_LiteralContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseTTQLVisitor) VisitOC_BooleanLiteral(ctx *OC_BooleanLiteralContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseTTQLVisitor) VisitOC_NumberLiteral(ctx *OC_NumberLiteralContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseTTQLVisitor) VisitOC_IntegerLiteral(ctx *OC_IntegerLiteralContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseTTQLVisitor) VisitOC_DoubleLiteral(ctx *OC_DoubleLiteralContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseTTQLVisitor) VisitOC_ListLiteral(ctx *OC_ListLiteralContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseTTQLVisitor) VisitOC_MapLiteral(ctx *OC_MapLiteralContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseTTQLVisitor) VisitOC_PropertyKeyName(ctx *OC_PropertyKeyNameContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseTTQLVisitor) VisitOC_Parameter(ctx *OC_ParameterContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseTTQLVisitor) VisitOC_SchemaName(ctx *OC_SchemaNameContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseTTQLVisitor) VisitOC_ReservedWord(ctx *OC_ReservedWordContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseTTQLVisitor) VisitOC_SymbolicName(ctx *OC_SymbolicNameContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseTTQLVisitor) VisitOC_LeftArrowHead(ctx *OC_LeftArrowHeadContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseTTQLVisitor) VisitOC_RightArrowHead(ctx *OC_RightArrowHeadContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseTTQLVisitor) VisitOC_Dash(ctx *OC_DashContext) interface{} {
	return v.VisitChildren(ctx)
}
