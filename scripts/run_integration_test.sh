#!/bin/bash

# This file sets up an testing environment providing a docker Neo4j DMBS and a docker TimescaleDB DMBS which run databases with
# simplistic testing data and runs the Golang SHALLOW and DEEP tests against it

SCRIPT=$(realpath "$0")
TTDB_SCRIPTS=$(dirname "$SCRIPT")

# stop all running docker containers to avoid conflicts 
docker stop $(docker ps -aq)

# rm testing containers 
docker rm test_neo4j
docker rm test_timescaledb
docker rm restore_neo4j 


# cleanup before (if docker-compose was run in other ways than this script there might be some leftovers)
sudo rm -rf $TTDB_SCRIPTS/../docker-test/neo4j/backups/*
ls $TTDB_SCRIPTS/../docker-test/neo4j/backups/
sudo rm -rf $TTDB_SCRIPTS/../docker-test/neo4j/data/*
ls $TTDB_SCRIPTS/../docker-test/neo4j/data/
sudo rm -rf $TTDB_SCRIPTS/../docker-test/timescaledb/backups/*
ls  $TTDB_SCRIPTS/../docker-test/timescaledb/backups/
sudo rm -rf $TTDB_SCRIPTS/../docker-test/timescaledb/data/*
ls $TTDB_SCRIPTS/../docker-test/timescaledb/data/


# prepare testing data 
cp $TTDB_SCRIPTS/../test-data/neo4j_test_backup/neo4j.dump $TTDB_SCRIPTS/../docker-test/neo4j/backups/
cp $TTDB_SCRIPTS/../test-data/timescaledb_test_backup/postgres.bak $TTDB_SCRIPTS/../docker-test/timescaledb/backups/

# restore neo4j data (was not able to get this to work with docker-compose yet)
docker run --name restore_neo4j --volume=$TTDB_SCRIPTS/../docker-test/neo4j/data:/data --volume=$TTDB_SCRIPTS/../docker-test/neo4j/backups:/backups neo4j neo4j-admin database load neo4j --from-path=/backups --verbose


# some rest for neo4j restore to settle; even checking for database status like we do below with the help of status request via neo4j-admin database info 
# does not seem to assure readiness of the database in any case  
sleep 4

# prepare testing envionment 
docker-compose -f $TTDB_SCRIPTS/../docker-compose.yml up -d


# wait for ports to be available 
dockerize -wait tcp://127.0.0.1:7687 -timeout 10s
dockerize -wait tcp://127.0.0.1:5432 -timeout 10s


# check if neo4j database is ready
docker exec test_neo4j sh -c "while neo4j-admin database info | grep -q 'Database in use:.*false'; do echo 'neo4j database not ready yet'; sleep 2; done"
echo "neo4j database ready"


# check if timescaledb database is ready
# Note: we cannot just check for the database name because the name "postgres" of the testing database
# is the same as the initial database and therefore cannot be differed just by its name. Therefore readiness cannot be implied without checking for content. 
while ! echo "\dt" | docker exec -i test_timescaledb psql | grep -qw ts_96a4656a_5de6_4807_8052_4546f2b0b291; 
do echo "timescaledb not ready yet"; sleep 2; 
done

echo "timescaledb database ready"

# Run Golang tests
go test -count=1 $TTDB_SCRIPTS/../api -run TestShallowQueries 
gotest1=$?
go test -count=1 $TTDB_SCRIPTS/../api -run TestDeepQueries 
gotest2=$?


# # stop docker compose - removes the containers
# docker-compose -f $TTDB_SCRIPTS/../docker-compose.yml down
# 
# # cleanup  
# sudo rm -rf $TTDB_SCRIPTS/../docker-test/neo4j/backups/*
# sudo rm -rf $TTDB_SCRIPTS/../docker-test/neo4j/data/*
# sudo rm -rf $TTDB_SCRIPTS/../docker-test/timescaledb/backups/*
# sudo rm -rf $TTDB_SCRIPTS/../docker-test/timescaledb/data/*

if [ $gotest1 -eq 0 ] && [ $gotest2 -eq 0 ]; then
    echo "Tests passed"
    exit 0
else
    if [ $gotest1 -ne 0 ]; then
        echo "Shallow tests failed"
    fi
    if [ $gotest2 -ne 0 ]; then
        echo "Deep tests failed"
    fi
    exit 1
fi
