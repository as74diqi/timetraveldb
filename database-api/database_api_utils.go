package databaseapi

func ClearData() {
	ClearNeo4j()
	ClearTimescale()
}
